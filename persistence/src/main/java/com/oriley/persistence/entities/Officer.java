package com.oriley.persistence.entities;

import java.util.Objects;

public class Officer {
	private Integer id;
	private Rank rank;
	private String first;
	private String last;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Rank getRank() {
		return rank;
	}

	public void setRank(Rank rank) {
		this.rank = rank;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public Officer() {
	}

	public Officer(Rank rank, String first, String last) {
		this.rank = rank;
		this.first = first;
		this.last = last;
	}

	public Officer(Integer id, Rank rank, String first, String last) {
		this.id = id;
		this.rank = rank;
		this.first = first;
		this.last = last;
	}

	@Override
	public String toString() {
		return "Officer [id=" + id + ", rank=" + rank + ", first=" + first + ", last=" + last + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(first, id, last, rank);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Officer other = (Officer) obj;
		return Objects.equals(first, other.first) && Objects.equals(id, other.id) && Objects.equals(last, other.last)
				&& rank == other.rank;
	}

}