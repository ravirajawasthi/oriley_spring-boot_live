package com.oriley.persistence.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.oriley.persistence.entities.Officer;
import com.oriley.persistence.entities.Rank;

@SpringBootTest
@Transactional
class JdbcOfficerDAOTest {
	
	Logger logger = LoggerFactory.getLogger(JdbcOfficerDAOTest.class);
	
	@Autowired
	private JdbcOfficerDAO dao;
	
	
	@Test
	void save() throws Exception {
		Officer officer = new Officer(Rank.LIEUTENANT, "Nyota", "Uhuru");
		officer = dao.save(officer);
		logger.info(officer.toString());
		assertNotNull(officer);
	}
	
	@Test
	void findByIfThatExist() throws Exception {
		Officer officer = new Officer(Rank.LIEUTENANT, "Nyota", "Uhuru");
		int id = dao.save(officer).getId();
		
		assertTrue(dao.existsById(id));
	}
	
	@Test
    void findOneThatDoesNotExist() throws Exception {
		Integer randomId = 99999999;
		assertFalse(dao.existsById(randomId));
	}
	
	@Test
    void count() throws Exception {
        assertThat(dao.count()).isEqualTo(5);
    }
	
	
	
	

}
