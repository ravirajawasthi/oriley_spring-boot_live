package com.orielly.restclient.services;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.orielly.restclient.entities.Site;

@SpringBootTest
class GeocoderServiceTest {
	Logger logger = LoggerFactory.getLogger(GeocoderServiceTest.class);

	@Autowired
	GeocoderService service;

	@Test
	void testGetLatLng() {
		Site site = service.getLatLng("Boston", "MA");
		logger.info(String.format("Lat and log for Boston MA is %f and %f", site.getLatitude(), site.getLongitude()));
		logger.info(site.toString());
		assertAll(() -> assertEquals(42.36, site.getLatitude(), 0.01),
				() -> assertEquals(-71.06, site.getLongitude(), 0.01));
	}

	@Test
	void getLatLngWithStreet() throws Exception {
		Site site = service.getLatLng("1600 Ampitheatre Parkway", "Mountain View", "CA");

		logger.info(String.format("Lat and log for Google CA office is %f and %f", site.getLatitude(),
				site.getLongitude()));
		logger.info(site.toString());
		assertAll(() -> assertEquals(37.42, site.getLatitude(), 0.01),
				() -> assertEquals(-122.08, site.getLongitude(), 0.01));
	}

}
