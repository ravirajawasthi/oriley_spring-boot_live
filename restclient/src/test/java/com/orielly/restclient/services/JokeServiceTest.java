package com.orielly.restclient.services;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;


@AutoConfigureWebTestClient(timeout = "36000")
@SpringBootTest
class JokeServiceTest {

	private Logger logger = LoggerFactory.getLogger(JokeServiceTest.class);

	@Autowired
	private JokeService service;

	@Test
	void getJoke() throws Exception {
		String joke = service.getJoke("Craig", "Walls");
		logger.info(joke);
		assertTrue(joke.contains("Craig") || joke.contains("Walls"));
	}

	@Test
	void getJokeRT() throws Exception {
		String joke = service.getJokeRT("Craig", "Walls");
		logger.info(joke);
		assertTrue(joke.contains("Craig") || joke.contains("Walls"));
	}
}
