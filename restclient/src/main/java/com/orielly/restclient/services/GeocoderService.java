package com.orielly.restclient.services;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.orielly.restclient.entities.Site;
import com.orielly.restclient.json.GeocoderResponse;

@Service
public class GeocoderService {
	private WebClient client;
	private static final String KEY = "AIzaSyDw_d6dfxDEI7MAvqfGXEIsEMwjC1PWRno";

	@Autowired
	public GeocoderService(WebClient.Builder builder) {
		client = builder.baseUrl("https://maps.googleapis.com").build();
	}

	public Site getLatLng(String... address) {
		// Creating string of addresses separated by (",") [comma]
		String encoded = Stream.of(address)
				.map((String component) -> URLEncoder.encode(component, StandardCharsets.UTF_8)) // Encoding string as
																									// per google api
				.collect(Collectors.joining(","));

		GeocoderResponse response = client.get()
				.uri(uriBuilder -> uriBuilder.path("/maps/api/geocode/json").queryParam("address", encoded)
						.queryParam("key", KEY).build())
				.retrieve().bodyToMono(GeocoderResponse.class).block(Duration.ofSeconds(2));
		
		return new Site(response.getFormattedAddress(), response.getLocation().getLat(),
				response.getLocation().getLng());
	}

}
