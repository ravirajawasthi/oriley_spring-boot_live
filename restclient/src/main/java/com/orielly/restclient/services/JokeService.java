package com.orielly.restclient.services;

import java.time.Duration;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.orielly.restclient.json.JokeResponse;

@Service // @Service and @Component is same

// Presentation layer -> services layer -> dao layer -> db
// presentation is for controller and views
// service for transaction boundaries and business logic
// dao layer converts java objects into database
public class JokeService {
	private WebClient client;
	private RestTemplate template;

	// if there is only one constructor, spring autowires its args automatically
	public JokeService(WebClient.Builder builder, RestTemplateBuilder restTemplateBuilder) {
		client = builder.baseUrl("http://api.icndb.com/").build();
		template = restTemplateBuilder.build();

	}

	public String getJoke(String first, String last) {
		return client.get()
				.uri(uriBuilder -> uriBuilder.path("/jokes/random").queryParam("limitTo", "[nerdy]")
						.queryParam("firstName", first).queryParam("lastName", last).build())
				.accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(JokeResponse.class) // Promise that response
																								// will only have 1tt
																								// class
				.map(jokeResponse -> jokeResponse.getValue().getJoke()).block(Duration.ofSeconds(2));
	}

	public String getJokeRT(String first, String last) {
		return template.getForObject(String
				.format("https://api.icndb.com/jokes/random?limitTo=[nerdy]&firstName=%s&lasyName=%s", first, last),
				JokeResponse.class).getValue().getJoke(); //getValue is from JokeResponse class
	}
}
