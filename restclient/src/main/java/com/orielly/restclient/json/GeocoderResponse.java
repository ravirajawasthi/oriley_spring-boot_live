package com.orielly.restclient.json;

import java.util.List;

public class GeocoderResponse {
    private List<GeocoderResult> results;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GeocoderResult> getResults() {
        return results;
    }

    public void setResults(List<GeocoderResult> results) {
        this.results = results;
    }

    public GeocoderLocation getLocation() {
        return results.get(0).getGeometry().getLocation();
    }

    public String getFormattedAddress() {
        return results.get(0).getFormattedAddress();
    }
}









