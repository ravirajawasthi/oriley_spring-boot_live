package com.orielly.restclient.json;

public class GeocoderGeometry {
	private GeocoderLocation location;

    public GeocoderLocation getLocation() {
        return location;
    }

    public void setLocation(GeocoderLocation location) {
        this.location = location;
    }
}
