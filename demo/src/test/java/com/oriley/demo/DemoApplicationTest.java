package com.oriley.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.text.NumberFormat;

import com.oriley.demo.entities.Greeting;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest
public class DemoApplicationTest {
    @Autowired
    private ApplicationContext context;

    @Autowired
    // Autowired based on _type_ (class or interface)
    private NumberFormat currentFormat;

    @Test
    void contextLoads() {
        assertNotNull(context);
        int count = context.getBeanDefinitionCount();
        System.out.println("There are " + count + " beans being managed");

    }

    @Test
    void greetintInAppCtx() {
        Greeting bean1 = context.getBean(Greeting.class);
        System.out.println(bean1);
        Greeting bean2 = context.getBean(Greeting.class);
        System.out.println(bean2);

        bean1.setMessage("Hello There!");
        assertSame(bean1, bean2); // spring saves only 1 instance

    }

    @Test
    void formatCurrency() {
        double amount = 123456789.14;
        System.out.println(currentFormat.format(amount));
    }

}
