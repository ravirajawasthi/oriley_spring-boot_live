package com.oriley.demo.controllers;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

public class HelloControllerUnitTest {

    @Test
    void sayHello() {
        HelloController controller = new HelloController();
        Model model = new BindingAwareModelMap();
        String viewName = controller.sayHello("SpringBootAuthor", model);

        assertAll(() -> assertEquals("SpringBootAuthor", model.asMap().get("user")),
                () -> assertEquals("hello", viewName));

    }
}
