package com.oriley.demo.entities;

// @Scope("singleton") // Spring will make a new bean everytime i ask, default is singleton
// @Component // if we remove this spring will not manage this, hence its bean will not be in
//            // applicationContext
public class Greeting {
    private String message;

    public Greeting() {
    }

    public Greeting(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
