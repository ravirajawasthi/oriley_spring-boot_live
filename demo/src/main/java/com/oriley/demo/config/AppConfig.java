package com.oriley.demo.config;

import java.text.NumberFormat;

import com.oriley.demo.entities.Greeting;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    public Greeting defaultGreeting() { // this function will enable singleton behaviour for Greeting class
        return new Greeting("Hello");
    }

    @Bean
    public NumberFormat getNumberFormatter() {
        return NumberFormat.getCurrencyInstance();
    }

}
