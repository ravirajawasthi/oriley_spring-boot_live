package com.oriley.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {
    @GetMapping(path = "/hello") // http://localhost:8080/hello?name=SpringBootAuthor (name is optional)
    public String sayHello(@RequestParam(required = false, defaultValue = "world") String name, Model model) {
        model.addAttribute("user", name); // bascially map for java
        return "hello"; // thmyleaf view resolver => resources/templeates/hello.html
    }
}
