package com.oreilly.shopping.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProductTest {

	@Autowired
	private Validator validator;

	@Test
	void autowiringWorked() {
		assertNotNull(validator);
		System.out.println(validator.getClass().getName());
	}

	@Test
	void nameCannotBeBlank() {
		Product product = new Product();
		Set<ConstraintViolation<Product>> violations = validator.validate(product);
		assertEquals(2, violations.size());

	}

	@Test
	void priceGEThanZero() {
		Product product = new Product("p1", new BigDecimal(-1.0));
		Set<ConstraintViolation<Product>> result = validator.validate(product);
		assertEquals(1, result.size());
		
		result.stream().forEach((res) -> {
			assertEquals("Price must be greater than or equal to zero", res.getMessage());
		});
	}

}
