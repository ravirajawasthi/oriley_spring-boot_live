package com.oreilly.shopping.dao;
import static org.junit.Assert.assertThrows;

import java.math.BigDecimal;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.oreilly.shopping.entities.Product;

@SpringBootTest
class ProductRepositoryTest {

	@Autowired
	private ProductRepository repo;

	@Test
	void saveProduct() {
		Product product = new Product(" ", new BigDecimal("1"));
		assertThrows(ConstraintViolationException.class, () -> repo.save(product));

	}

}
