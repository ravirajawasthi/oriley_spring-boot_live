package com.oreilly.shopping.controllers;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.oreilly.shopping.entities.Product;
import com.oreilly.shopping.services.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {

	private final ProductService service;

	@Autowired
	public ProductController(ProductService service) {
		this.service = service;
	}

	@GetMapping()
	public List<Product> findAllProducts() {
		return service.findAllProducts();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Product> findProductById(@PathVariable(name = "id") int id) {
		return ResponseEntity.of(service.findById(id));
	}

	@PostMapping()
	public ResponseEntity<Product> createProduct(@RequestBody Product product) {
		Product savedProduct = service.createProduct(product);
		URI location = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}")
				.buildAndExpand(savedProduct.getId()).toUri();

		return ResponseEntity.created(location).body(savedProduct);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Void> deleteProduct(@PathVariable Integer id){
		service.deleteProductById(id);
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping()
	public void deleteAllProducts() {
		service.deleteAllProducts();
	}
}
