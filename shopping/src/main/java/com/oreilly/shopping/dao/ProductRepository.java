package com.oreilly.shopping.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.oreilly.shopping.entities.Product;


public interface ProductRepository extends JpaRepository<Product, Integer> {

	// Product - The class we are saving
	// Integer - The class of primary key

}
