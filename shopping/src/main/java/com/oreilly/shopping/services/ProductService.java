package com.oreilly.shopping.services;

import static org.assertj.core.api.Assertions.assertThatIllegalStateException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oreilly.shopping.dao.ProductRepository;
import com.oreilly.shopping.entities.Product;

@Service
@Transactional
public class ProductService {

	Logger log = LoggerFactory.getLogger(ProductService.class);

	private final ProductRepository repository;

	@Autowired
	public ProductService(ProductRepository repository) {
		this.repository = repository;
	}

	public void initializeDatabase() {

		if (this.repository.count() == 0)
			this.repository.saveAll(List.of(new Product("baseball", new BigDecimal("5.99")),
					new Product("football", new BigDecimal("5.99")),
					new Product("baseketball", new BigDecimal("5.99")))).forEach(p -> log.info(p.toString()));

	}

	public List<Product> findAllProducts() {
		return this.repository.findAll();
	}

	public Optional<Product> findById(Integer id) {
		return this.repository.findById(id);
	}
	
	
	//Returns a saved product with generated id
	public Product createProduct(Product product) {
		return this.repository.save(product);

	}
	
	
	public void deleteProductById(Integer id) {
		this.repository.deleteById(id);
	}
	
	
	public void deleteAllProducts() {
		this.repository.deleteAllInBatch();
	}
	
}
